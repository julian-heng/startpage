#!/usr/bin/env python3

from typing import Any, Dict, List, Optional, Union

import base64
import sys

from lxml import etree, html
from pyquery import PyQuery as pq
from yaml import safe_load as yaml_load


def base64encode(path: str) -> str:
    with open(path, "rb") as file_stream:
        return base64.b64encode(file_stream.read()).decode("ascii")


def read_file(path: str) -> str:
    with open(path, "r") as file_stream:
        return file_stream.read()


def gen_tag(tag_name: str, tag_attr: Optional[Dict[str, str]] = None) -> pq:
    # pylint: disable=E1137
    result: pq = pq("<{0}></{0}>".format(tag_name))
    encode_flag: bool = False
    data: Optional[str] = None

    if tag_attr is not None and tag_attr:
        for key, value in tag_attr.items():
            if key == "encoded":
                encode_flag = True
            elif key == "file":
                data = base64encode(value)
            elif key == "href" and encode_flag and data is not None:
                result.attr[key] = value.format(data)
            elif key == "html":
                result.html(value)
            else:
                result.attr[key] = value
    return result


def gen_header(header: List[Union[Any, Dict[str, Any]]],
               fonts: List[Union[Any, Dict[str, Any]]]) -> pq:
    result: pq = gen_tag("head")
    stylesheets: List[str] = list()
    fonts_encode: Dict[str, str] = dict()

    style: pq = gen_tag("style")
    font: str = ""
    style_content: str = ""

    for i in header:
        for tag, attr in i.items():
            if ("rel" in attr
                    and attr["rel"] == "stylesheet"
                    and attr["encoded"]):
                stylesheets.append(attr["href"])
            else:
                result.append(gen_tag(tag, attr))

    for i in fonts:
        for key, value in i.items():
            fonts_encode[key] = base64encode(value)

    for key, value in fonts_encode.items():
        font = ("@font-face {{" +
                "    font-family: {};" +
                "    src: url(data:font/truetype;charset=utf-8;base64,{}) " +
                "    format(\"truetype\");" +
                "}}\n").format(key, value)
        style_content = "{}\n{}".format(style_content, font)

    for i in stylesheets:
        style_content = "{}\n{}".format(style_content, read_file(i))

    style.append(style_content)
    result.append(style)

    return result


def gen_link_box(link: Dict[str, Any]) -> pq:
    result: pq = gen_tag("div", {"id": link["header"].lower(),
                                 "class": "link-box"})
    header: pq = gen_tag("h3").append(link["header"])
    header = gen_tag("div", {"class": "header"}).append(header)

    links_element: pq = gen_tag("ul")
    for key, value in link["urls"].items():
        links_element.append(gen_tag("li").append(gen_tag("a", {"href": value,
                                                                "html": key})))

    result.append(header + links_element)
    return result


def gen_body(links: List[Union[Any, Dict[str, Any]]]) -> pq:
    result: pq = gen_tag("body")

    row_limit: int = 4
    row_count: int = 1
    count: int = 0

    box: pq = gen_tag("div", {"id": "box", "class": "flex-container"})
    row: pq = gen_tag("div", {"id": "row{}".format(row_count),
                              "class": "flex-container"})

    for count, link in enumerate(links):
        if count != 0 and count % row_limit == 0:
            box.append(row)
            row_count += 1
            row = gen_tag("div", {"id": "row{}".format(row_count),
                                  "class": "flex-container"})
        row.append(gen_link_box(link))

    if count % row_limit != 0:
        box.append(row)

    result.append(box)
    return result


def main():
    config: Dict[Any, Any] = None
    header: pq = None
    body: pq = None
    result: pq = None
    document: str = ""

    with open("config.yml") as file_stream:
        config = yaml_load(file_stream)
    if config is None:
        sys.exit(1)

    header = gen_header(config["header"], config["fonts"])
    body = gen_body(config["links"])

    result = gen_tag("html")
    result.append(header)
    result.append(body)

    document = html.fromstring(str(result))
    with open("index.html", "w") as file_stream:
        document = etree.tostring(document,
                                  encoding="unicode",
                                  pretty_print=True)
        file_stream.write(document)


main()
